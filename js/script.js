const llamandoFetch = () => {
    const url = "servicio.json";
  
    fetch(url)
      .then((respuesta) => respuesta.json())
      .then((datos) => mostrarDatos(datos))
      .catch((reject) => {
        console.log("Ocurrió un error");
      });
  };
  
  const mostrarDatos = (datos) => {
    const table = document.getElementById("table");
    const result = document.getElementById("res");
    var totalGanancia=0
  
    for (let item of datos) {
      const row = table.insertRow();
      row.insertCell().innerHTML = item.codigo;
      row.insertCell().innerHTML = item.idcliente;
      row.insertCell().innerHTML = item.descripcion;
      row.insertCell().innerHTML = item.cantidad;
      row.insertCell().innerHTML = item.preciovta;
      row.insertCell().innerHTML = item.preciocompra;
      row.insertCell().innerHTML = (item.preciovta * item.preciocompra)*item.cantidad.toFixed(2);
      let ganancia =  (parseInt(item.preciovta)*parseInt(item.preciocompra))*parseInt(item.cantidad)
      totalGanancia += ganancia
      
    }

    result.innerHTML += totalGanancia;

  };
  
  document.getElementById("btnCargar").addEventListener("click", function () {
    llamandoFetch();
  });